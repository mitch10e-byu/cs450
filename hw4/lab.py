import cv2
import cv2.cv as cv
import os
import numpy as np
from matplotlib import pyplot as plt
from scipy import ndimage

in_dir = 'in/'
out_dir = 'out/'
file_type = '.png'


def check_dir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)


def load_image(filename):
    image = cv2.imread(in_dir + filename + file_type, 0)
    return image


def save_image(filename, image):
    plt.clf()
    plt.axis('off')
    plt.imshow(image, cmap='gray')
    # plt.title(filename)
    plt.savefig(out_dir + filename)


def init():
    check_dir(in_dir)
    check_dir(out_dir)


def my_edge_detection(filename, image):
    save_image('test', image)

    dx = np.matrix('-1 0 1; -2 0 2; -1 0 1')
    filtered_x = ndimage.convolve(image, dx, mode='constant')
    # filtered_x = ndimage.filters.sobel(image, 1)
    save_image('sobel-x-' + filename, filtered_x)

    dy = np.matrix('1 2 1; 0 0 0; -1 -2 -1')
    filtered_y = ndimage.convolve(image, dy, mode='constant')
    # filtered_y = ndimage.filters.sobel(image, 0)
    save_image('sobel-y-' + filename, filtered_y)

    magnitude = np.sqrt(filtered_x ** 2 + filtered_y ** 2)
    save_image('magnitude-' + filename, magnitude)

    orientation = np.arctan2(filtered_x, filtered_y)
    save_image('orientation-' + filename, orientation)

    laplacian_kernel = np.matrix('0 1 0; 1 -4 1; 0 1 1')
    laplacian_filter = ndimage.convolve(magnitude, laplacian_kernel)
    save_image('laplacian-' + filename, laplacian_filter)

    zero_crossings = zero_crossings_from_laplacian(laplacian_filter)
    save_image(filename + '-zero-crossing', zero_crossings)

def set_pixel_crossing(pixel, right, down):
    if pixel > 0 and (right < 0 or down < 0):
        return 1
    if pixel < 0 and (right > 0 or down > 0):
        return 1
    return 0


def zero_crossings_from_laplacian(image):
    height, width = image.shape
    result = np.zeros((width, height))
    for y in range(0, height):
        for x in range(0, width):
            pixel = image[x][y]
            if x + 1 < width:
                right = image[x + 1][y]
            else:
                right = image[x][y]

            if y + 1 < height:
                down = image[x][y + 1]
            else:
                down = image[x][y]
            result[x][y] = set_pixel_crossing(pixel, right, down)
    return result


def run_part_a_with_image_cv(filename, image):
    sobel_x = cv2.Sobel(image, cv2.CV_64F, 1, 0, ksize=5)
    sobel_y = cv2.Sobel(image, cv2.CV_64F, 0, 1, ksize=5)
    magnitude = cv2.Canny(image, 100, 200)
    orientation = np.arctan2(sobel_x, sobel_y)
    laplacian = cv2.Laplacian(image, cv2.CV_64F, ksize=3)

    save_image('cv-' + filename + '-laplacian', laplacian)
    save_image('cv-' + filename + '-sobel-x', sobel_x)
    save_image('cv-' + filename + '-sobel-y', sobel_y)
    save_image('cv-' + filename + '-magnitude', magnitude)
    save_image('cv-' + filename + '-orientation', orientation)

    zero_crossings = zero_crossings_from_laplacian(laplacian)
    save_image('cv-' + filename + '-zero-crossing', zero_crossings)


def find_circles_with_size_cv(filename, image, size):
    image = cv2.medianBlur(image, 5)
    detected = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    circles = cv2.HoughCircles(image, cv.CV_HOUGH_GRADIENT, 2, 20, np.array([]), 50, 30, size - 2, size + 2)
    if circles is not None:
        for circle in circles[0, :]:
            RED = (int(circle[0]), 0, 0)
            GREEN = (0, int(circle[0]), 0)
            BLUE = (0, 0, int(circle[0]))
            cv2.circle(detected, (circle[0], circle[1]), circle[2], RED, 2)
            cv2.circle(detected, (circle[0], circle[1]), 1, BLUE, 2)
            # print "Center: " + str(circle[0]) + ", " + str(circle[1]) + " Radius: " + str(circle[2])
    save_image('cv-' + filename, detected)


def locate_circles(accumulator, radius, height, width, image, filename):
    # print "locating circles"
    circles = np.zeros((width * height, 3))
    # number of votes needed compared to best circle (out of 255)
    cutoff = 144
    for x in range(0, width):
        for y in range(0, height):
            value = accumulator[x][y]
            if value > circles[0][0] and value > cutoff:
                circles[0][0] = np.ceil(value)
                circles[0][1] = x
                circles[0][2] = y
                ind = np.lexsort((circles[:, 1], circles[:, 0]))
                circles = circles[ind]
    # print circles
    # print image

    detected = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    hough_maxima = np.zeros((width, height))
    for circle in circles:
        GREEN = (0, int(circle[0]), 0)
        BLUE = (0, 0, int(circle[0]))
        # print circle
        if circle[0] > cutoff:
            # circle
            cv2.circle(detected, (int(circle[2]), int(circle[1])), radius, GREEN, 2)
            # center
            cv2.circle(detected, (int(circle[2]), int(circle[1])), 1, BLUE, 2)
        hough_maxima[circle[1]][circle[2]] = circle[0]
    save_image(filename, detected)
    save_image('hough-maxima-' + filename, hough_maxima)


def my_hough(filename, image, radius):
    print filename
    padding = radius
    image = cv2.copyMakeBorder(image, padding, padding, padding, padding, cv2.BORDER_REPLICATE, value=[0, 0, 0])
    height, width = image.shape

    # pad image to find edge circles
    # height += radius * 2
    # width += radius * 2

    accumulator = np.zeros((height, width))
    # print height, width
    image = cv2.medianBlur(image, 5)
    edge_threshold = 60
    edge_diff = 1
    image = cv2.Canny(image, edge_threshold, edge_threshold + edge_diff)
    save_image('before-' + filename + str(radius), image)
    # exit(1)
    voting_accuracy = 360
    for x in range(0, width):
        for y in range(0, height):
                if image[x][y] == 255:
                    # This is where the choice between efficiency and speed is made, default = 360
                    for angle in range(0, voting_accuracy):
                        rho = angle * np.pi / (voting_accuracy / 2)
                        x0 = np.round(x - radius * np.cos(rho))
                        y0 = np.round(y - radius * np.sin(rho))
                        if width > x0 > 0:
                            if height > y0 > 0:
                                accumulator[x0][y0] += 1

    # print "finding max"
    # max_value = 0
    # for x in range(0, width):
    #     for y in range(0, height):
    #         if accumulator[x][y] > max_value:
    #             max_value = accumulator[x][y]

    # print "normalizing"
    # for x in range(0, width):
    #     for y in range(0, height):
    #         value = (accumulator[x][y] / max_value) * 255.0
    #         accumulator[x][y] = value

    max_value = np.amax(accumulator)
    # print max_value
    if max_value > 128:
        accumulator *= 255 / max_value
    locate_circles(accumulator, radius, width, height, image, filename)


def run_part_a_with_image(filename, image):
    run_part_a_with_image_cv(filename, image)
    my_edge_detection(filename, image)


def run_part_b_with_image(filename, image):
    find_circles_with_size_cv(filename + '-small', image, 16)
    find_circles_with_size_cv(filename + '-medium', image, 32)
    find_circles_with_size_cv(filename + '-large', image, 48)

    my_hough(filename + '-small-my-hough', image, 16)
    my_hough(filename + '-medium-my-hough', image, 32)
    my_hough(filename + '-large-my-hough', image, 48)


def preprocess(image):
    image = cv2.blur(image, (2, 2))
    image = cv2.fastNlMeansDenoising(image, h=5)
    return image


def part_a():
    whitebox = load_image('whitebox')
    run_part_a_with_image('whitebox', whitebox)
    blocks = load_image('blocks')
    blocks = preprocess(blocks)
    run_part_a_with_image('blocks', blocks)


def part_b():
    simple = load_image('simplecircle')
    run_part_b_with_image('simple', simple)
    circles = load_image('circles')
    run_part_b_with_image('circles', circles)


init()
part_a()
part_b()