from PIL import Image
from kernel import Kernel
from matplotlib import pyplot as plt
import numpy as np
import math


def get_image_data(img):
    width, height = img.size
    pixels = img.load()
    return width, height, pixels


def get_gray_image():
    img = Image.open('out/grayscale.png')
    return img


# Gray =  0.299 * Red  +  0.587 * Green  +  0.114 * Blue
def grayscale(img):
    # print "grayscale"
    width, height, pixels = get_image_data(img)
    for row in range(0, height):
        for col in range(0, width):
            gray = int(math.ceil(
                (.299 * pixels[col, row][0]) +
                (.587 * pixels[col, row][1]) +
                (.114 * pixels[col, row][2])
            ))
            pixels[col, row] = (gray, gray, gray)


def brighten(img, value):
    width, height, pixels = get_image_data(img)
    for row in range(0, height):
        for col in range(0, width):
            pixel = pixels[col, row]
            bright = pixel[0] + value
            if bright < 0:
                bright = 0
            elif bright > 255:
                bright = 255
            pixels[col, row] = (bright, bright, bright)


def blur(img, blur_type):
    kernal = Kernel()
    width, height, pixels = get_image_data(img)
    for row in range(0, height):
        for col in range(0, width):
            top = max(row - 1, 0)
            bot = min(row + 1, height - 1)
            left = max(col - 1, 0)
            right = min(col + 1, width - 1)
            kernal.set_mask(
                pixels[left, top][0], pixels[col, top][0], pixels[right, top][0],
                pixels[left, row][0], pixels[col, row][0], pixels[right, row][0],
                pixels[left, bot][0], pixels[col, bot][0], pixels[right, bot][0])
            if blur_type is "uniform":
                pixels[col, row] = (kernal.mean(), kernal.mean(), kernal.mean())
            elif blur_type is "median":
                pixels[col, row] = (kernal.median(), kernal.median(), kernal.median())


def sharpen(img):
    kernal = Kernel()
    width, height, pixels = get_image_data(img)
    for row in range(0, height):
        for col in range(0, width):
            top = max(row - 1, 0)
            bot = min(row + 1, height - 1)
            left = max(col - 1, 0)
            right = min(col + 1, width - 1)
            kernal.set_mask(
                0, -pixels[col, top][0], 0,
                -pixels[left, row][0], 5 * pixels[col, row][0], -pixels[right, row][0],
                0, -pixels[col, bot][0], 0)
            value = int(math.ceil(kernal.sum() / 2))
            if value < 0:
                value = 0
            elif value > 255:
                value = 255
            pixels[col, row] = (value, value, value)


def edge_detection(img):
    kernal_x = Kernel()
    kernal_y = Kernel()
    temp = img.copy()
    temp = temp.load()
    width, height, pixels = get_image_data(img)
    for row in range(0, height):
        for col in range(0, width):
            top = max(row - 1, 0)
            bot = min(row + 1, height - 1)
            left = max(col - 1, 0)
            right = min(col + 1, width - 1)
            kernal_x.set_mask(
                -temp[left, top][0], 0, temp[right, top][0],
                -temp[left, row][0] * 2, 0, temp[right, row][0] * 2,
                -temp[left, bot][0], 0, temp[right, bot][0]
            )
            kernal_y.set_mask(
                -temp[left, top][0], -temp[col, top][0] * 2, temp[right, top][0],
                0, 0, 0,
                temp[left, bot][0], temp[col, bot][0] * 2, temp[right, bot][0]
            )
            x = kernal_x.sum()
            y = kernal_y.sum()
            value = int(math.sqrt((x * x) + (y * y)))
            value = (255 - value) + 128
            if value < 0:
                value = 0
            elif value > 255:
                value = 255
            pixels[col, row] = (value, value, value)


def histogram_equalization(img):
    bins = 256
    x_limits = [0, 256]
    width, height, pixels = get_image_data(img)
    data_array = []
    # print data
    for row in range(0, height):
        for col in range(0, width):
            pixel = pixels[col, row][0]
            data_array.append(pixel)
    data_array_np = np.array(data_array)
    # print data_array_np
    histogram, bins = np.histogram(data_array_np.flatten(), bins, x_limits)
    cdf = histogram.cumsum()
    # print histogram
    # normal = cdf * histogram.max() / cdf.max()
    # show_histogram(normal, data_array_np, bins, x_limits)
    cdf_mask = np.ma.masked_equal(cdf, 0)
    # print cdf_mask
    area = cdf_mask.max() - cdf_mask.min()
    cdf_mask = (cdf_mask - cdf_mask.min()) * 255 / (cdf_mask.max() - cdf_mask.min())
    cdf = np.ma.filled(cdf_mask, 0).astype('uint8')
    equalized = cdf[img]
    for row in range(0, height):
        for col in range(0, width):
            # print row, col
            # print cdf[img][row, col]
            pixels[col, row] = (equalized[row, col][0], equalized[row, col][1], equalized[row, col][2])


def show_histogram(normal, data_array_np, bins, x_limits):
    plt.plot(normal, color='red')
    plt.hist(data_array_np.flatten(), bins, x_limits, color='blue')
    plt.xlim(x_limits)
    plt.legend(('cdf', 'histogram'), loc='upper left')
    plt.show()


image_directory = "img/"
file_name = image_directory + 'reggie.jpg'

image = Image.open(file_name)
print "grayscale"
grayscale(image)
image.save('out/grayscale.png')

image = get_gray_image()
print "brighten"
brighten(image, 50)
image.save('out/brighten.png')

image = get_gray_image()
print "uniform blur"
blur(image, "uniform")
image.save('out/uniform_blur.png')

image = get_gray_image()
print "median blur"
blur(image, "median")
image.save('out/median_blur.png')

image = get_gray_image()
print "sharpen"
sharpen(image)
image.save('out/sharpen.png')

image = get_gray_image()
print "edge detection"
edge_detection(image)
image.save('out/edge_detection.png')

image = get_gray_image()
print "histogram equalization"
histogram_equalization(image)
image.save('out/histogram.png')
