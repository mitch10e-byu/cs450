import math


class Kernel:
    def __init__(self):
        self.mask = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ]

    def set_mask(self, k11, k12, k13, k21, k22, k23, k31, k32, k33):
        self.mask = [
            [k11, k12, k13],
            [k21, k22, k23],
            [k31, k32, k33]
        ]

    def get_mask(self):
        print self.mask

    def sum(self):
        total = 0
        for i in range(0, 3):
            for j in range(0, 3):
                total += self.mask[i][j]
        return total

    def mean(self):
        return int(math.ceil(self.sum() / 9))

    def median(self):
        values = []
        for i in range(0, 3):
            for j in range(0, 3):
                values.append(self.mask[i][j])
        values.sort()
        return values[4]