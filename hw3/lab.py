import pylab
import argparse
import numpy as np
from math import *
import pickle as pkl
from PIL import Image
import numpy.fft as fft
from logger import Logger
import matplotlib.pylab as plt
from scipy import signal as sgnl


def get_signal_from_image(file_name):
    image = Image.open(file_name).convert('L')
    width = image.size[0]
    height = image.size[1]

    signal = []
    for y in range(0, height):
        signal.append([])
        for x in range(0, width):
            signal[y].append(image.getpixel((x, y)))
    return signal, width, height


def filter_2D(frequency, filt):
    G = []
    index = 0
    for F, H in zip(frequency, filt):
        G.append([])
        for x, y in zip(F, H):
            G[index].append(x * y)
        index += 1
    return to_spatial_domain2(G)


def filter_1D(frequency, filt):
    G = []
    for F, H in zip(frequency, filt):
        G.append(F * H)
    return to_spatial_domain(G)


def butterworth_filter_1D(length):
    filt = []
    for x in range(0, length):
        filt.append(1 / (1 + (x ** 2 / length)))
    return filt


def gaussian_filter_1D(length, sigma=1):
    filt = []
    for x in range(0, length):
        base = 1 / np.sqrt(2 * np.pi * (sigma ** 2))
        exponent = -(x ** 2) / (2 * (sigma ** 2))
        formula = base * np.exp(exponent)
        filt.append(formula)
    return filt


def gaussian_filter_2D(width, height=None, sigma=1, high=False):
    filt = []
    if height is None:
        height = width
    for y in range(0, height):
        filt.append([])
        for x in range(0, width):
            base = 1 / np.sqrt(2 * np.pi * (sigma ** 2))
            exponent = (-((x ** 2) + (y ** 2)) / (2 * (sigma ** 2)))
            formula = base * np.exp(exponent)
            if high:
                filt[y].append(1 - formula)
            else:
                filt[y].append(formula)
    return filt


def precision(value):
    return float("{:.5f}".format(value))


def to_spatial_domain(sig):
    sig = fft.ifftshift(sig)
    return fft.ifft(sig)


def to_freq_domain(sig):
    F = fft.fft(sig)
    return fft.fftshift(F)


def to_spatial_domain2(sig):
    sig = fft.ifftshift(sig)
    return fft.ifft2(sig)


def to_freq_domain2(sig):
    F = fft.fft2(sig)
    return fft.fftshift(F)


def save_image_1D(data, name, folder='out/'):
    n = len(data)
    plt.clf()
    x = np.linspace(0.0, n, num=n, endpoint=False)
    plt.plot(x, data)
    plt.savefig("%s%s.png" % (folder, name), cmap=plt.get_cmap('gray'))


def save_image(image, name, folder='out/'):
    plt.clf()
    plt.imshow(np.asarray(image), cmap=pylab.gray())
    plt.savefig("%s%s.png" % (folder, name), cmap=plt.get_cmap('gray'))


def save_before_after(before, after, name, folder='out/'):
    plt.subplot(2, 1, 1)
    plt.imshow(before, cmap=pylab.gray())
    plt.subplot(2, 1, 2)
    plt.imshow(after, cmap=pylab.gray())
    plt.savefig("%s%s.png" % (folder, name))


def sine2dx(freq, n=256):
    sig = []
    for y in range(n):
        sig.append([])
        for x in range(n):
            pixel = sin(2 * pi * freq * x / n)
            sig[y].append(pixel)
    return sig


def sine2dy(freq, n=256):
    sig = []
    for y in range(n):
        sig.append([])
        for x in range(n):
            pixel = sin(2 * pi * freq * y / n)
            sig[y].append(pixel)
    return sig


def magnitude1D(sinusoid):
    n = len(sinusoid)
    fourier = fft.fft2(sinusoid)
    fourier = fft.fftshift(fourier)
    mag = []
    for y in range(n):
        mag.append([])
        for x in range(n):
            pixel = sqrt(fourier[x][y].real ** 2 + fourier[x][y].imag ** 2)
            mag[y].append(pixel)
    return mag


def magnitude2D(frequency, width, height):
    magnitude = []
    for y in range(0, height):
        magnitude.append([])
        for x in range(0, width):
            magnitude[y].append((sqrt(frequency[y][x].real ** 2 + frequency[y][x].imag ** 2) / (width * height)))
    return magnitude


# create a 'padded kernel' - should x,y coordinate be 1 or 0?
def in_kernel(width, height, x, y):
    size = 9
    inKernel = False
    if x + y < size:
        inKernel = True
    elif width - x + y - 1 < size:
        inKernel = True
    elif x + height - y - 1 < size:
        inKernel = True
    elif width - x + height - y - 2 < size:
        inKernel = True
    return inKernel


class Lab:
    def __init__(self):
        self.log = Logger()
        self.signals = pkl.load(open('in/HW3_PartB.pkl', 'r'))
        self.args = []
        self.parse_args()
        self.run()

    def parse_args(self):
        parser = argparse.ArgumentParser(prog='Fourier Transform Lab 2D',
                                         description='CS 450 Fall 2015 Lab 3, Fourier Transform 2D',
                                         add_help=True)
        parser.add_argument('-d', '--debug', action='store_true', help='enable [DEBUG]')
        parser.add_argument('-i', '--info', action='store_true', help='enable [INFO ]')
        parser.add_argument('-w', '--warn', action='store_true', help='enable [WARN ]')
        self.args = parser.parse_args()
        if self.args.debug:
            self.log.enable_debug()
        if self.args.info:
            self.log.enable_info()
        if self.args.warn:
            self.log.enable_warn()

    def run(self):
        self.partA()
        self.partB()
        self.partC()
        self.partD()
        self.partE()

    def partA(self):
        self.log.info("A - Simple Sines & Cosines")
        folder = 'out/a/'
        for i in range(0, 3):
            s1 = (i + 1) * 3
            # self.log.debug("SineX with s=%i" % s1)
            sinex = sine2dx(s1)
            sine_magnitude = magnitude1D(sinex)
            save_image(sinex, 'sinex-%i' % s1, folder)
            save_image(sine_magnitude, 'sinex_magnitude-%i' % s1, folder)

            s2 = s1 + 1
            # self.log.debug("SineY with s=%i" % s2)
            siney = sine2dy(s2)
            siney_magnitude = magnitude1D(siney)
            save_image(siney, 'siney-%i' % s2, folder)
            save_image(siney_magnitude, 'siney_magnitude-%i' % s2, folder)
        self.log.info("A - COMPLETE")

    def partB(self):
        folder = 'out/b/'
        self.log.info("B - 1D Filtering")
        signal = self.signals
        length = len(signal)
        frequency = to_freq_domain(signal)

        gaussian = gaussian_filter_1D(length, 12)
        gaussian = to_freq_domain(gaussian)

        butterworth = butterworth_filter_1D(length)
        butterworth = to_freq_domain(butterworth)

        gaussian_smoothing = filter_1D(frequency, gaussian)
        butterworth_smoothing = filter_1D(frequency, butterworth)

        save_image_1D(signal, 'signal', folder)
        save_image_1D(frequency.real, 'frequency', folder)
        save_image_1D(gaussian.real, 'gaussian', folder)
        save_image_1D(gaussian_smoothing.real, 'gaussian_smoothing', folder)
        save_image_1D(butterworth.real, 'butterworth', folder)
        save_image_1D(butterworth_smoothing.real, 'butterworth_smoothing', folder)
        self.log.info("B - COMPLETE")

    def partC(self):
        self.log.info("C - 2D Filtering & Convolution Theorem")
        folder = 'out/c/'
        image = plt.imread('in/whitebox.png')
        kernel = np.ones((9, 9))
        kernel /= 81

        r = sgnl.convolve2d(image[:, :, 0], kernel, mode='same')
        g = sgnl.convolve2d(image[:, :, 1], kernel, mode='same')
        b = sgnl.convolve2d(image[:, :, 2], kernel, mode='same')

        imageOut = np.dstack([r, g, b])
        imageOut = (imageOut * 255).astype(np.uint8)
        save_before_after(image, imageOut, 'example-domain-filter', 'out/c/')

        # End Example - Spatial Domain Filter

        image = Image.open('in/whitebox.png').convert('L')
        width = image.size[0]
        height = image.size[1]
        signal = []
        kernel = []
        # 0 pad kernel, read in pixels
        for y in range(0, height):
            signal.append([])
            kernel.append([])
            for x in range(0, width):
                signal[y].append(image.getpixel((x, y)))
                if in_kernel(width, height, x, y):
                    kernel[y].append(1)
                else:
                    kernel[y].append(0)
        frequency = to_freq_domain2(signal)
        kernel_freq = to_freq_domain2(kernel)

        filtered = filter_2D(frequency, kernel_freq)

        save_image(np.abs(frequency), 'frequency', folder)
        save_image(kernel, 'kernel', folder)
        save_image(np.abs(kernel_freq), 'kernel_frequency', folder)
        save_image(np.abs(filtered), 'filtered_signal', folder)
        save_before_after(signal, np.abs(filtered), 'before_after', folder)
        self.log.info("C - COMPLETE")

    def partD(self):
        self.log.info("D - Magnitude, Phase & Filtering")
        folder = 'out/d/'

        ball_signal, width, height = get_signal_from_image('in/ball.png')
        gull_signal, width, height = get_signal_from_image('in/gull.png')

        ball_freq = to_freq_domain2(ball_signal)
        gull_freq = to_freq_domain2(gull_signal)

        ball_magnitude = []
        ball_phase = []
        ball_mix = []

        gull_magnitude = []
        gull_phase = []
        gull_mix = []

        for y in range(0, height):
            ball_magnitude.append([])
            ball_phase.append([])
            ball_mix.append([])

            gull_magnitude.append([])
            gull_phase.append([])
            gull_mix.append([])
            for x in range(0, width):
                ball_magnitude[y].append(np.sqrt((ball_freq[x][y].real ** 2) + (ball_freq[x][y].imag ** 2)))
                ball_phase[y].append(np.arctan2(ball_freq[x][y].imag, ball_freq[x][y].real))

                gull_magnitude[y].append(np.sqrt((gull_freq[x][y].real ** 2) + (gull_freq[x][y].imag ** 2)))
                gull_phase[y].append(np.arctan2(gull_freq[x][y].imag, gull_freq[x][y].real))

                ball_mix[y].append(
                    complex(ball_magnitude[y][x] * cos(gull_phase[y][x]), ball_magnitude[y][x] * sin(gull_phase[y][x])))
                gull_mix[y].append(
                    complex(gull_magnitude[y][x] * cos(ball_phase[y][x]), gull_magnitude[y][x] * sin(ball_phase[y][x])))

        ball_out = to_spatial_domain2(ball_mix)
        gull_out = to_spatial_domain2(gull_mix)

        ball_image_out = Image.new('L', (width, height))
        gull_image_out = Image.new('L', (width, height))

        ball_out_pix = ball_image_out.load()
        gull_out_pix = gull_image_out.load()

        for y in range(0, height):
            for x in range(0, width):
                ball_out_pix[x, y] = np.abs(ball_out[x][y])
                gull_out_pix[x, y] = np.abs(gull_out[x][y])

        lpf = gaussian_filter_2D(width, height, sigma=12)
        lpf_freq = to_freq_domain2(lpf)

        ball_lpf = filter_2D(ball_freq, lpf_freq)
        gull_lpf = filter_2D(gull_freq, lpf_freq)

        hpf = gaussian_filter_2D(width, height, sigma=12, high=True)
        hpf_freq = to_freq_domain2(hpf)

        ball_hpf = filter_2D(ball_freq, hpf_freq)
        gull_hpf = filter_2D(gull_freq, hpf_freq)

        save_before_after(ball_signal, ball_image_out, 'mixed-up-ball', folder)
        save_before_after(gull_signal, gull_image_out, 'mixed-up-gull', folder)
        save_before_after(ball_image_out, gull_image_out, 'mixed-up', folder)
        save_image(lpf, 'low-pass-filter', folder)
        save_image(hpf, 'high-pass-filter', folder)
        save_image(np.abs(lpf_freq), 'low-pass-filter-freq', folder)
        save_image(np.abs(hpf_freq), 'high-pass-filter-freq', folder)
        save_before_after(np.abs(ball_lpf), np.abs(ball_hpf), 'ball-lpf-hpf', folder)
        save_before_after(np.abs(gull_lpf), np.abs(gull_hpf), 'gull-lpf-hpf', folder)
        self.log.info("D - COMPLETE")

    def partE(self):
        self.log.info("E - Interference Pattern")
        folder = 'out/e/'
        signal, width, height = get_signal_from_image('in/interfere.png')
        frequency = to_freq_domain2(signal)
        magnitude = magnitude2D(frequency, width, height)
        save_image(np.abs(magnitude), 'magnitude', folder)
        save_image(np.abs(frequency), 'frequency', folder)
        for y in xrange(height):
            for x in xrange(width):
                if (x - 1) < 0 or (y - 1) < 0 or (x + 1 > width / 2) or (y + 1 > height / 2):
                    pass
                else:
                    center = magnitude[x][y]
                    dir1 = magnitude[x + 1][y]
                    dir2 = magnitude[x - 1][y]
                    dir3 = magnitude[x][y - 1]
                    dir4 = magnitude[x][y + 1]
                    avg = (dir1 + dir2 + dir3 + dir4) / 4
                    if center > (avg * 4):
                        dir1 = frequency[x + 1][y]
                        dir2 = frequency[x - 1][y]
                        dir3 = frequency[x][y - 1]
                        dir4 = frequency[x][y + 1]
                        avg = (dir1 + dir2 + dir3 + dir4) / 4
                        frequency[x][y] = avg
                        frequency[width - x][height - y] = avg

        out_signal = to_spatial_domain2(frequency)
        save_image(signal, 'signal', folder)
        save_image(np.abs(frequency), 'frequency2', folder)
        save_image(np.abs(out_signal), 'out_signal', folder)
        self.log.info("E - COMPLETE")


if __name__ == '__main__':
    lab = Lab()
