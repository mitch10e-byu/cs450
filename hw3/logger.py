RESET = '\033[0m'
RED = '\033[31m'
BLUE = '\033[34m'
GREEN = '\033[32m'
YELLOW = '\033[33m'

DEBUG = '[DEBUG]\t\t'
INFO = '[INFO ]\t\t'
WARN = '[WARN ]\t\t'
ERROR = '[ERROR]\t\t'

class Logger:
    def __init__(self):
        self.enableDebug = False
        self.enableInfo = False
        self.enableWarn = False

    @staticmethod
    def print_message(message, color):
        print color + message + RESET

    def enable_debug(self):
        self.enableDebug = True
        # self.debug("Debug Enabled")

    def enable_info(self):
        self.enableInfo = True
        # self.info("Info Enabled")

    def enable_warn(self):
        self.enableWarn = True
        # self.warn("Warn Enabled")

    def debug(self, message):
        if self.enableDebug:
            self.print_message(DEBUG + message, GREEN)

    def info(self, message):
        if self.enableInfo:
            self.print_message(INFO + message, BLUE)

    def warn(self, message):
        if self.enableWarn:
            self.print_message(WARN + message, YELLOW)

    def err(self, message):
        self.print_message(ERROR + message, RED)
