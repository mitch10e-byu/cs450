#!/usr/bin/python

from numpy import *
from numpy.fft import *
from scipy.misc import *

def rgb2gray(rgb):
    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray

# Setup/preprocessing
# Read the raw image
filename = 'input.png'
f_t = imread(filename)

# Convert image to a gray floating-point (double) image in range 0.0 to 1.0
if f_t.ndim == 3:
    f_t = double(rgb2gray(f_t) )/255.0
else:
    f_t = double(f_t)/255.0

# Forward FFT
# Do the forward FFT and shift to be zero-centered
F_u = fftshift( fft2(f_t) )

# Now create your transfer function H_u in the frequncy domain
H_u = zeros( F_u.shape, dtype='complex' )
#H_u = <<< Fill in your filter here >>>


# Do frequency space filtering here...
G_u = zeros( F_u.shape, dtype='complex' )
G_u = F_u * H_u # for example given a transfer function H_u


# Inverse FFT
# Convert back to spatial domain
g_t = ifft2( ifftshift(G_u) )


# Post-processing in spatial domain
imsave( 'output.png', uint8(real(g_t)*255) )




