import cv2
import numpy as np
import os
from PIL import Image
import PIL.ImageDraw
from random import randrange
# import matplotlib.pyplot as plt

in_dir = 'in/'
out_dir = 'out/'
file_type = '.png'

sceneImages = ['campus1_1200', 'campus2_1200', 'campus3_1200']


def init():
    print "Init"
    check_directory(in_dir)
    check_directory(out_dir)


# SIFT
def part_a():
    print "Part A"
    scene = load_scene(sceneImages)
    keypoint_images = []
    keypoints = []
    descriptors = []
    for x in range(0, len(scene)):
        keypoints.append([])
        descriptors.append([])
        k = find_keypoints(scene[x])
        keypoint_image = cv2.drawKeypoints(scene[x], k, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        keypoint_images.append(keypoint_image)
        k, d = find_descriptors(scene[x], k)
        keypoints[x] = k
        descriptors[x] = d
    save_images('keypoints', keypoint_images)
    return scene, keypoints, descriptors


# Matching
def part_b(scene, keypoints, descriptors):
    print "Part B"
    matches = find_matches(descriptors)
    print "creating matched image"
    matched_image = np.zeros((scene[0].shape[1] + scene[1].shape[1], scene[0].shape[0]))
    for y in range(0, scene[0].shape[0]):
        for x in range(0, scene[0].shape[0]):
            matched_image[y][x] = scene[0][y][x]
            matched_image[y + scene[0].shape[0]][x] = scene[1][y][x]
    # print matched_image
    matched_image = draw_matches(matched_image, matches, keypoints)


    # plt.imshow(matched_image), plt.show()
    save_images('matched', [matched_image])
    return matches


# Homographies & RANSAC
def part_c(scene, keypoints, descriptors, matches):
    print "Part C"
    if len(matches) > 10:
        src_pts = np.float32([keypoints[1][m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)
        dst_pts = np.float32([keypoints[0][m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)

        H, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 100.0)
        print H
        # offset
        H[0:2, 2] += (0, -scene[1].shape[1] / 18)
        # warp = cv2.warpPerspective(scene[1], H, (scene[1].shape[0] * 2, scene[1].shape[1] * 2))
        warp = cv2.warpPerspective(scene[0], -H, (scene[0].shape[0], scene[0].shape[1]))
        save_images('warp', [warp])
        return warp, H


# Stitching
def part_d(scene, keypoints, descriptors, matches, warped, H):
    print "Part D"
    panorama = blend_images(warped, scene[1], H)
    save_images('panorama', [panorama])


def check_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def save_images(prefix, images):
    print "saving images: " + prefix
    for x in range(0, len(images)):
        cv2.imwrite(out_dir + prefix + '-' + str(x) + file_type, images[x])


def load_scene(image_file_names):
    print "loading scene"
    images = []
    for name in image_file_names:
        print in_dir + name + file_type
        image = cv2.imread(in_dir + name + file_type, 0)
        images.append(image)
    return images


def find_keypoints(image):
    print "finding keypoints"
    sift = cv2.SIFT()
    keypoints = sift.detect(image, None)
    return keypoints


def find_descriptors(image, keypoints):
    print "finding descriptors"
    sift = cv2.SIFT()
    keypoints, descriptors = sift.compute(image, keypoints)
    return keypoints, descriptors


def find_matches(descriptors):
    print "finding matches"
    print "match"
    bfmatcher = cv2.BFMatcher()

    threshold = 0.25
    matches = bfmatcher.knnMatch(descriptors[0], descriptors[1], k=2)
    good = []
    for m, n in matches:
        if m.distance < threshold * n.distance:
            good.append([m])

    reverse_matches = bfmatcher.knnMatch(descriptors[1], descriptors[0], k=2)
    reverse_good = []
    for m, n in reverse_matches:
        if m.distance < threshold * n.distance:
            reverse_good.append([m])

    best_matches = []
    for i in range(len(good)):
        for j in range(len(reverse_good)):
            if good[i][0].queryIdx == reverse_good[j][0].trainIdx:
                if reverse_good[j][0].queryIdx == good[i][0].trainIdx:
                    best_matches.append(good[i][0])

    best_matches = sorted(best_matches, key=lambda x: x.distance)
    if len(best_matches) > 100:
        best_matches = best_matches[:50]

    return best_matches


def draw_matches(image, matches, keypoints):
    print "drawing matches"
    image = Image.fromarray(image)
    draw = PIL.ImageDraw.Draw(image)
    for l in range(0, len(matches)):
        point1 = (keypoints[0][matches[l].queryIdx].pt[0], keypoints[0][matches[l].queryIdx].pt[1])
        point2 = (keypoints[1][matches[l].trainIdx].pt[0], keypoints[1][matches[l].trainIdx].pt[1] + 1200)
        draw.line((point1, point2), fill=0)
    return np.array(image)


    # [X']   [h11 h12 h13][X]
    # [Y'] = [h21 h22 h23][Y]
    # [1 ]   [h31 h32 1  ][1]


def homography(p1, p2):
    print "homography"
    m_index = 0
    H = np.zeros((8, 9))
    for i in range(0, len(p1)):
        x = p1[i][0]
        y = p1[i][1]
        u = p2[i][0]
        v = p2[i][1]
        H[m_index] = [0, 0, 0, -x, -y, -1, v * x, v * y, v]
        H[m_index + 1] = [x, y, 1, 0, 0, 0, -u * x, -u * y, -u]
        m_index += 2
    U, S, V = np.linalg.svd(H, full_matrices=True)
    matrix = V[:, 8].reshape(3, 3)
    return matrix


def blend_images(base, warp, H):
    print "blending images"
    panorama_height = base.shape[0]
    panorama_width = base.shape[1] * 2
    panorama = np.zeros((panorama_height, panorama_width))
    for y in range(panorama_height):
        for x in range(panorama_width):
            if x < panorama_width / 2:
                panorama[y][x] = base[y][x]
            else:
                panorama[y][x] = warp[y][x - base.shape[0]]
    return panorama


init()
s, k, d = part_a()
m = part_b(s, k, d)
w, H = part_c(s, k, d, m)
part_d(s, k, d, m, w, H)
