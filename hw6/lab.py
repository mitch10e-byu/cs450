import os
import cv2
import maxflow
from graph import *
from scipy import misc
from matplotlib import pyplot as plt

in_dir = 'in/'
out_dir = 'out/'
file_type = '.png'
# name = 'simplecircle'
# name = 'redblue'
name = 'zebra'
# name = 'butterfly'
# name = 'simpleRects'
# name = 'rectangles'

picking_background = False
background_seed = ()
object_seed = ()


def check_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def load_image(img_name):
    print "Loading Image: " + img_name
    img = misc.imread(in_dir + img_name + file_type)
    return img


def onclick(event):
    print 'button=%d, x=%d, y=%d, xdata=%f, ydata=%f'%(
        event.button, event.x, event.y, event.xdata, event.ydata)


def create_graph_maxflow(img):
    print "Creating Graph (Maxflow Version)"
    g = maxflow.Graph[int]()
    nodeids = g.add_grid_nodes(img.shape)
    # print nodeids
    g.add_grid_edges(nodeids, 50)
    g.add_grid_tedges(nodeids, img, 255 - img)
    g.maxflow()
    segments = g.get_grid_segments(nodeids)
    img2 = np.int_(np.logical_not(segments))
    # plt.imshow(img2)
    # plt.show()
    plt.imsave(out_dir + name + "_test" + file_type, img2)

def create_graph(img, seeds):
    print "creating graph"
    print "background seed: " + str(seeds[0])
    print "object seed:     " + str(seeds[1])
    sigma = 5
    g = maxflow.Graph[int]()
    nodes = g.add_nodes(img.shape[0] * img.shape[1])
    for y in range(img.shape[1]):
        for x in range(image.shape[0]):
            if x + 1 < image.shape[0]:
                pixel_difference = np.abs((img[x][y] - img[x + 1][y]))
            else:
                pixel_difference = np.abs((img[x][y] - img[x - 1][y]))
            boundary_term = np.exp((-1 * (pixel_difference * pixel_difference) / 2 * sigma))
            boundary_term = np.average(boundary_term)
            # print boundary_term
            if y + 1 < image.shape[1]:
                # print "adding edge down (two way)"
                g.add_edge(nodes[y * img.shape[0] + x], nodes[(y + 1) * img.shape[0] + x], boundary_term, boundary_term)
            if x + 1 < image.shape[0]:
                # print "adding edge right (two way)"
                g.add_edge(nodes[y * img.shape[0] + x], nodes[y * img.shape[0] + x + 1], boundary_term, boundary_term)
            # print img[y][x], seeds[1], seeds[0]
            foreground_probability = seeds[1] - img[x][y]
            background_probability = seeds[0] - img[x][y]
            foreground = foreground_probability / (foreground_probability + background_probability)
            background = background_probability / (foreground_probability + background_probability)
            # print foreground_probability, background_probability
            # print foreground, background
            g.add_tedge(nodes[y * img.shape[0] + x], np.mean(background), np.mean(foreground))
    return g, nodes


def get_pixel(event, x, y, flags, param):
    global picking_background, background_seed, object_seed
    if event == cv2.EVENT_LBUTTONDOWN:
        print y, x, image[y, x]
        if picking_background:
            picking_background = False
            background_seed = image[y, x]
            print "background seed set to: " + str(background_seed)
        else:
            picking_background = True
            object_seed = image[y, x]
            print "object seed set to: " + str(object_seed)


def open_picker(img):
    print "Pick seed pixels"
    cv2.namedWindow('Seed Picker')
    cv2.setMouseCallback('Seed Picker', get_pixel)
    print "Object"
    while True:
        cv2.imshow('Seed Picker', img)
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()


def run(graph, nodes):
    print "running maxflow"
    graph.maxflow()
    segments = graph.get_grid_segments(nodes)
    result = np.int_(np.logical_not(segments))
    # print result
    # plt.imshow(result, cmap='Greys')
    # plt.show()
    return result

def save(result, name):
    print "saving " + name
    plt.imshow(result, cmap='Greys')
    plt.imsave(out_dir + name + file_type, result, cmap='Greys')


def save_with_original(result, image, name):
    print "saving " + name + " with original"
    out_image = np.zeros(image.shape)
    for y in range(result.shape[1]):
        for x in range(result.shape[0]):
            if result[x][y] != 0:
                out_image[x][y] = image[x][y]
            else:
                if len(out_image[x][y]) == 3:
                    out_image[x][y] = [0, 0, 0]
                elif len(out_image[x][y]) == 4:
                    out_image[x][y] = [0, 0, 0, 0]

    misc.imsave(out_dir + name + "_original" + file_type, out_image)

check_dir('in')
check_dir('out')

image = load_image(name)
# create_graph_maxflow(image)
create_graph_maxflow(image)


open_picker(image)
graph, nodes = create_graph(image, [background_seed, object_seed])
# print image.shape
# print image.shape[0] * image.shape[1]
# print len(nodes)
np.set_printoptions(threshold=np.nan)
nodes = np.reshape(nodes, (image.shape[0], image.shape[1]))
result = run(graph, nodes)
save(result, name)
save_with_original(result, image, name)