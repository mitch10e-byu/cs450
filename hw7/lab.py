import argparse
import glob
from logger import Logger
import numpy as np
import cv2
from sklearn.cluster import KMeans
import pickle
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import SVC
import math
from sklearn.metrics import precision_recall_curve, average_precision_score
import matplotlib.pyplot as plt

file_type = '.jpg'
head_folder = 'ObjectCategories/'
image_file = 'image_'
out = 'out/'
# image_classes = ['accordion', 'anchor', 'barrel', 'dragonfly', 'elephant', 'garfield', 'lotus']
image_classes = ['accordion', 'anchor', 'barrel', 'dragonfly', 'elephant', 'garfield', 'lotus', 'menorah', 'pagoda']


class Detector:
    def __init__(self):
        self.testing_keys = []
        self.testing_histograms = []
        self.training_keys = []
        self.training_histograms = []
        self.feature_vectors = dict()
        self.clusters = []
        self.descriptors = []
        self.images = []
        self.logger = Logger()
        self.parse_arguments()
        self.num_clusters = 400

    def parse_arguments(self):
        parser = argparse.ArgumentParser(prog='Object Detection', description='CS 450 Object Detection Lab', add_help=True)
        parser.add_argument('-d', '--debug', action='store_true', help='enable [DEBUG] prints')
        parser.add_argument('-i', '--info', action='store_true', help='enable [INFO ] prints')
        args = parser.parse_args()
        if args.debug:
            self.logger.enable_debug()
        if args.info:
            self.logger.enable_info()

    def run(self):
        self.logger.info("Starting Lab")
        for i in range(0, len(image_classes)):
            self.read_files(image_classes[i])
        self.sift_files()
        clusters = self.cluster()
        # clusters = self.open_pickle(self.get_cluster_name())
        self.create_histograms(clusters)
        self.create_svm(clusters)

    def get_cluster_name(self):
        name = ""
        for i in range(0, len(image_classes)):
            name += image_classes[i] + "-"
        return name

    def pickle_object(self, name, object):
        self.logger.info("Pickling Objects: " + name)
        pickle.dump(object, open(name + "cluster.pkl", "wb"))

    def open_pickle(self, name):
        self.logger.info("Opening Pickle Jar: " + name)
        object = pickle.load(open(name + "cluster.pkl", "rb"))
        return object

    def read_files(self, image_type):
        self.logger.info('Reading Files: ' + image_type)
        num_images = len(glob.glob1(head_folder + image_type + "/", image_file + "*" + file_type))
        self.logger.debug(image_type + " contains " + str(num_images) + " images")
        for i in range(0, num_images):
            self.read_file(head_folder + image_type + "/" + image_file + format(i + 1, '04') + file_type)

    def read_file(self, file_name):
        self.logger.debug('Reading File: ' + file_name)
        image = cv2.imread(file_name)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        self.images.append(image)

    def sift_files(self):
        self.logger.info('SIFT images')
        sift = cv2.SIFT()
        for i in range(0, len(self.images)):
            self.logger.debug('image #' + str(i + 1))
            kp = sift.detect(self.images[i], None)
            kp, d = sift.compute(self.images[i], kp)
            for i in range(0, len(d)):
                self.descriptors.append(d[i])
        self.logger.info('done')

    def cluster(self):
        self.logger.info('Beginning k-means clustering')
        m = 10000
        # m can be > 100000, but my computer cannot handle much more than 20000 in a reasonable amount of time
        n = len(self.descriptors)
        samples = np.random.permutation(n)[0:m]
        self.logger.debug("samples: " + str(samples) + ' out of ' + str(n) + ' possible')
        X = []
        for i in range(0, len(samples)):
            X.append(self.descriptors[samples[i]])
        clusters = KMeans(n_clusters=self.num_clusters)
        clusters.fit(X)
        self.pickle_object(self.get_cluster_name(), clusters)
        return clusters

    def create_histograms(self, clusters):
        self.logger.info("Creating Histograms")
        for i in range(0, len(image_classes)):
            self.feature_vectors[image_classes[i]] = []
        # load image for each image class
        # get descriptors
        # predict
        # np.histogram
        # add to dictionary
            images = []
            num_images = len(glob.glob1(head_folder + image_classes[i] + "/", image_file + "*" + file_type))
            self.logger.debug(image_classes[i] + " contains " + str(num_images) + " images")
            for j in range(0, num_images):
                file_name = head_folder + image_classes[i] + "/" + image_file + format(j + 1, '04') + file_type
                image = cv2.imread(file_name)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                images.append(image)
            for k in range(0, len(images)):
                d = self.get_descriptors_for_image(images[k])
                predictions = clusters.predict(d)
                histogram = np.histogram(predictions, bins=[l for l in range(0, self.num_clusters + 1)], normed=True)
                histogram = histogram[0] * 100
                self.feature_vectors[image_classes[i]].append(histogram)

    def get_descriptors_for_image(self, image):
        sift = cv2.SIFT()
        kp = sift.detect(image, None)
        kp, d = sift.compute(image, kp)
        return d

    def create_training_testing_sets(self):
        self.logger.info("Creating Training and Testing Sets")
        for key in self.feature_vectors:
            np.random.shuffle(self.feature_vectors[key])
            num_histograms = len(self.feature_vectors[key])
            training_amount = int(math.ceil(num_histograms / 2))
            # print key + ": " + str(training_amount) + " / " + str(num_histograms)
            for t in range(0, training_amount):
                self.training_histograms.append(self.feature_vectors[key][t])
                self.training_keys.append(key)
            for t in range(training_amount, len(self.feature_vectors[key])):
                self.testing_histograms.append(self.feature_vectors[key][t])
                self.testing_keys.append(key)
        # for z in range(len(self.training_keys)):
        #     print self.training_keys[z], self.training_histograms[z]



    def create_svm(self, clusters):
        self.logger.info("Creating SVM")
        self.create_training_testing_sets()
        SVM = OneVsRestClassifier(SVC(probability=True), n_jobs=-3)
        SVM.fit(self.training_histograms, self.training_keys)
        predictions = SVM.predict(self.testing_histograms)
        # print predictions
        # print np.array(self.testing_keys)

        results = []
        for r in range(len(predictions)):
            if predictions[r] == self.testing_keys[r]:
                results.append(True)
            else:
                results.append(False)
        y_test = []
        for t in range(len(self.training_keys)):
            y_test.append([])
            for u in range(len(image_classes)):
                if self.training_keys[t] == image_classes[u]:
                    y_test[t].append(1)
                else:
                    y_test[t].append(0)
        y_score = SVM.fit(self.training_histograms, self.training_keys).decision_function(self.testing_histograms)
        y_test = np.array(y_test)

        precision = dict()
        recall = dict()
        average_precision = dict()

        for i in range(len(image_classes)):
            precision[i], recall[i], _ = precision_recall_curve(y_test[:, i], y_score[:, i])
            average_precision[i] = average_precision_score(y_test[:, i], y_score[:, i])

        precision["micro"], recall["micro"], _ = precision_recall_curve(y_test.ravel(), y_score.ravel())
        average_precision["micro"] = average_precision_score(y_test, y_score, average="micro")

        # Plot Precision-Recall curve
        plt.clf()
        plt.plot(recall[0], precision[0], label='Precision-Recall curve')
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.ylim([0.0, 1.05])
        plt.xlim([0.0, 1.0])
        plt.title('Precision-Recall example: AUC={0:0.2f}'.format(average_precision[0]))
        # plt.legend(loc="best")
        # plt.show()
        plt.savefig(self.get_cluster_name() + "-pr1.jpg")

        # Plot Precision-Recall curve for each class
        plt.clf()
        plt.plot(recall["micro"], precision["micro"],
             label='Precision-recall curve (area = {0:0.2f})'
                   ''.format(average_precision["micro"]))
        for i in range(len(image_classes)):
            plt.plot(recall[i], precision[i],
                 label='class {0} (area = {1:0.2f})'
                       ''.format(i, average_precision[i]))

        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.title('Extension of Precision-Recall curve to multi-class')
        # plt.show()
        plt.savefig(self.get_cluster_name() + "-pr-curves.jpg")
        plt.legend(loc="best")
        plt.savefig(self.get_cluster_name() + "-pr-curves-legend.jpg")

if __name__ == "__main__":
    detector = Detector()
    try:
        detector.run()
    except KeyboardInterrupt:
        pass