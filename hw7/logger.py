class Logger:
    def __init__(self):
        self.enableDebug = False
        self.enableInfo = False

    def enable_debug(self):
        self.enableDebug = True
        self.debug("Debug Enabled")

    def enable_info(self):
        self.enableInfo = True
        self.info("Info Enabled")

    def debug(self, message):
        if self.enableDebug:
            print "\033[37m[DEBUG]\t\t" + message + "\033[0m"

    def info(self, info):
        if self.enableInfo:
            print "\033[34m[INFO ]\t\t" + info + "\033[0m"

    @staticmethod
    def err(error):
        print "\033[31m[ERROR]\t\t" + error + "\033[0m"

    def print_dictionary(self, dictionary):
        if self.enableInfo:
            for key, value in dictionary.iteritems():
                print "\033[32m[INFO ]\t\t" + key + ": " + value + "\033[0m"