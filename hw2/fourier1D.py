import numpy
from numpy.fft import *
import matplotlib.pyplot as plt


def get_sqwave(M):
    H = numpy.floor(M / 2.0)
    return numpy.hstack((numpy.ones((1, H)), numpy.zeros((1, H))))[0, :]


def get_double_sqwave(M):
    H = numpy.floor(M / 2.0)
    return numpy.hstack((get_sqwave(H), get_sqwave(H)))


def get_cos_wave(M, freq=1):
    x = numpy.linspace(0.0, 1.0, num=M, endpoint=False)
    return numpy.cos(2 * numpy.pi * freq * x)


def get_fourier_basis(M):
    cos_waves = []
    sin_waves = []
    x = numpy.linspace(0.0, 1.0, num=M, endpoint=False)
    for u in range(M):
        cos_waves.append(numpy.cos(2 * numpy.pi * u * x))
        sin_waves.append(numpy.sin(2 * numpy.pi * u * x))
    return cos_waves, sin_waves


def demo_dft_on_signal(f):
    M = f.size

    x = numpy.linspace(0.0, 1.0, num=M, endpoint=False)
    COS, SIN = get_fourier_basis(M)

    F = fft(f) / M  # because the numpy FFT is a factor of M off
    # from how we defined the DFT.

    fig = plt.figure(1)
    plt.subplot(2, 2, 1)
    a = plt.gca()
    a.clear()
    print x.shape
    print f.shape
    plt.plot(x, f, 'b.-')
    a.set_xlim(0, 1.0)
    a.set_ylim(-1.1, 1.1)
    plt.show(block=False)

    accum = numpy.zeros(f.shape)
    for u in range(M):
        F_u = F[u]
        cospart = F_u.real
        sinpart = F_u.imag
        mag = numpy.abs(F_u)
        phase = numpy.angle(F_u)
        if mag > 0:
            print 'F[%d] = %s  WHICH IS [%2.2fCOS + %2.2fSIN] or %2.2fMAG@%2.2fPHASE' % (
            u, F[u], cospart, sinpart, mag, phase)
        else:
            print 'F[%d] = 0' % u
        # plt.plot(

        xx = numpy.linspace(0, 1, 500)
        yy = numpy.cos(2 * numpy.pi * u * xx)
        plt.subplot(2, 2, 3)
        a = plt.gca()
        a.clear()
        plt.plot(xx, yy, 'k-')
        plt.plot(x, COS[u], 'c.-')
        plt.plot(x, SIN[u], 'r.-')
        a.set_xlim(0, 1.0)
        a.set_ylim(-1.1, 1.1)

        plt.subplot(2, 2, 4)
        a = plt.gca()
        a.clear()
        plt.plot(x, cospart * COS[u], 'c.-')
        plt.plot(x, sinpart * SIN[u], 'r.-')
        plt.plot(x, cospart * numpy.ones(x.shape), 'c-.')
        plt.plot(x, sinpart * numpy.ones(x.shape), 'r-.')
        uwave = cospart * COS[u] - sinpart * SIN[u]
        plt.plot(x, uwave, 'b.-')
        a.set_xlim(0, 1.0)
        a.set_ylim(-1.1, 1.1)

        accum += uwave

        plt.subplot(2, 2, 2)
        a = plt.gca()
        a.clear()
        plt.plot(x, accum, 'b.-')
        a.set_xlim(0, 1.0)
        a.set_ylim(-1.1, 1.1)
        plt.show(block=False)
        raw_input()
