import argparse
from logger import Logger
import numpy as np
import numpy.fft as fft
import math
import matplotlib.pylab as plt
import pickle as pkl


def precision(value):
    return float("{:.5f}".format(value))


def plot_parts(signal, name, mag=False, power=False, folder='out/', show=False):
    fourier = dft(signal)
    n = len(fourier)

    real = np.empty(n)
    imag = np.empty(n)

    for i in range(0, n):
        real[i] = fourier[i].real
        imag[i] = fourier[i].imag

    m = magnitude(fourier)
    p = phase(fourier)

    plt.clf()
    plt.figure(2)
    plt.title(name)
    x = np.linspace(0.0, 1.0, num=n, endpoint=False)
    plt.subplot(2, 1, 1)
    plt.plot(x, real, label='real', color='red', linewidth=1)
    plt.plot(x, imag, label='imag', color='green', linewidth=1)
    plt.legend(loc='lower right')
    plt.subplot(2, 1, 2)
    if mag:
        plt.plot(x, m, label='mag', color='orange', linewidth=1)
    plt.plot(x, p, label='phase', color='blue', linewidth=1)
    plt.legend(loc='lower right')
    if show:
        plt.show()
    plt.savefig('%s%s.png' % (folder, name))


def plot_signals(signals, name, label, folder='out/'):
    plt.clf()
    for i in range(0, len(signals)):
        n = len(signals[i])
        x = np.linspace(0.0, 1.0, num=n, endpoint=False)
        plt.plot(x, np.real(signals[i]), label=label[i])
    plt.title(name)
    plt.legend(loc='upper right')
    plt.savefig('%s%s.png' % (folder, name))


def dual_plot(signal1, signal2, title, label1, label2, folder='out/'):
    plt.clf()
    n = len(signal1)
    x = np.linspace(0.0, 1.0, num=n, endpoint=False)
    plt.title(title)
    plt.figure(1)
    plt.subplot(2, 1, 1)
    plt.plot(x, signal1, label=label1)
    plt.legend(loc='upper right')
    plt.subplot(2, 1, 2)
    plt.plot(x, signal2, label=label2)
    plt.legend(loc='upper right')
    plt.savefig('%s%s.png' % (folder, title))

def plot_mag_power(signal, mag, power, name, folder='out/'):
    n = len(signal)
    x = np.linspace(0.0, 1.0, num=n, endpoint=False)
    plt.clf()
    plt.title(name)
    plt.subplot(3, 1, 1)
    plt.ylabel(name)
    plt.plot(x, signal.real, label='Signal')
    plt.ylabel('Signal')
    plt.subplot(3, 1, 2)
    plt.plot(x, mag.real, label='Magnitude')
    plt.ylabel('Magnitude')
    plt.subplot(3, 1, 3)
    plt.plot(x, power.real, label='Power')
    plt.ylabel('Power')
    plt.savefig('%s%s.png' % (folder, name))


def sine(n=128, s=8):
    t = np.linspace(0.0, 1.0, num=n, endpoint=False)
    return np.sin(2 * np.pi * s * t)


def cosine(n=128, s=8):
    t = np.linspace(0.0, 1.0, num=n, endpoint=False)
    return np.cos(2 * np.pi * s * t)


def combine_waves(sin, cos):
    n = len(sin)
    combination = np.empty(n)
    for i in range(0, n):
        combination[i] = sin[i] + cos[i]
    return combination


def magnitude(signal):
    n = len(signal)
    temp = np.empty(n)
    for i in range(0, n):
        temp[i] = np.sqrt((signal[i].real ** 2) + (signal[i].imag ** 2))
    return temp


def phase(signal):
    n = len(signal)
    temp = np.empty(n)
    for i in range(0, n):
        temp[i] = np.arctan2(signal[i].imag, signal[i].real)
    return temp


def power_spectrum(signal):
    n = len(signal)
    temp = np.empty(n)
    for i in range(0, n):
        temp[i] = signal[i] ** 2
    return temp


def significant_frequencies(signal):
    n = len(signal)
    temp = []
    for i in range(1, n / 2 - 1):
        if signal[i - 1] < signal[i] and signal[i] > signal[i + 1]:
            temp.append(signal[i])
    temp = np.array(temp)
    return temp

def dft(array, normalize=True, clean=True):
    m = len(array)
    base = (2 * np.pi) / m
    f = np.empty(len(array), dtype=complex)
    for u in range(0, m):
        real = 0
        imag = 0
        for x in range(0, m):
            real += array[x] * np.cos(u * x * base)
            imag -= array[x] * np.sin(u * x * base)
        if normalize:
            real /= m
            imag /= m
        if clean:
            real = precision(real)
            imag = precision(imag)
        f[u] = complex(real, imag)
    return f


class Lab:
    def __init__(self):
        self.log = Logger()
        self.parse_args()
        self.args = []
        self.signal = np.array([1, 1, 0, 0])
        self.log.info("Loading Signals")
        self.signals = pkl.load(open('HW3_signals.pkl', 'r'))
        self.run()

    def parse_args(self):
        parser = argparse.ArgumentParser(prog='Fourier Transform Lab',
                                         description='CS 450 Fall 2015 Lab 2, Fourier Transform',
                                         add_help=True)
        parser.add_argument('-d', '--debug', action='store_true', help='enable [DEBUG]')
        parser.add_argument('-i', '--info', action='store_true', help='enable [INFO ]')
        parser.add_argument('-w', '--warn', action='store_true', help='enable [WARN ]')
        self.args = parser.parse_args()
        if self.args.debug:
            self.log.enable_debug()
        if self.args.info:
            self.log.enable_info()
        if self.args.warn:
            self.log.enable_warn()

    def run(self):
        self.partA()
        self.partB()
        self.partC()
        self.partD()
        self.partE()
        self.partF()

    def partA(self):
        self.log.info("PART A")
        self.log.info("Signal               : " + str(self.signal))
        self.log.debug("PY DFT               : " + str(fft.fft(self.signal)))
        self.log.info("My DFT               : " + str(dft(self.signal, False, True)))
        self.log.info("My DFT Normalized    : " + str(dft(self.signal, True, True)))
        self.log.info("")

    def partB(self):
        self.log.info("PART B")
        n2 = 9
        s2 = 5
        folder = 'out/b/'
        for i in range(4, n2):
            n = 2 ** i
            for j in range(2, s2):
                s = 2 ** j
                self.log.info('Sine--n=%is=%i' % (n, s))
                sin = sine(n, s)
                plot_parts(sin, 'Sine--n=%is=%i' % (n, s), True, True, folder)

                self.log.info('Cosine--n=%is=%i' % (n, s))
                cos = cosine(n, s)
                plot_parts(cos, 'Cosine--n=%is=%i' % (n, s), True, True, folder)

                self.log.info('Combination--n=%is=%i' % (n, s))
                combo = combine_waves(sin, cos)
                plot_parts(combo, 'Combination--n=%is=%i' % (n, s), True, True, folder)

        self.log.info("")

    def partC(self):
        self.log.info("PART C")
        self.log.info("Rect Signal")
        signal = self.signals['rect']
        # self.log.debug("RECT\n" + str(rect))
        signal_dft = dft(signal)
        signal_magnitude = magnitude(signal_dft)
        signal_power = power_spectrum(signal_magnitude)
        plot_mag_power(signal_dft, signal_magnitude, signal_power, 'Rect', 'out/c/')
        self.log.info("")

    def partD(self):
        self.log.info("PART D")
        self.log.info("Gaussian Signal")
        signal = self.signals['gaussian']
        signal_dft = dft(signal)
        signal_magnitude = magnitude(signal_dft)
        signal_power = power_spectrum(signal_magnitude)
        plot_mag_power(signal_dft, signal_magnitude, signal_power, 'Gaussian', 'out/d/')
        self.log.info("")

    def partE(self):
        self.log.info("PART E")
        signal = self.signals['test']
        signal_dft = dft(signal)
        signal_frequencies = significant_frequencies(signal_dft)
        plot_signals([signal_frequencies], 'signal', ['signal'], 'out/e/')
        signal_sorted = np.sort(signal_frequencies)
        n = len(signal_sorted)
        self.log.debug('non-zero frequencies = %i' % n)
        high_frequencies = np.empty(n)
        for i in range(0, n):
            high_frequencies[i] = signal_sorted[n - i - 1].real
        for i in range(0, 3):
            self.log.info("%i: %s" % (i + 1, str(high_frequencies[i])))
        self.log.info("")

    def partF(self):
        self.log.info("PART F")
        signal = self.signals['rect']
        signal_dft = fft.fft(signal) # f

        output = self.signals['output']
        output_dft = fft.fft(output) # g

        n = len(signal_dft)

        reals = np.empty(n)
        inverse = np.empty(n, dtype=complex)

        for i in range(0, n):
            if signal_dft[i] < 0.00001:
                reals[i] = 0
                inverse[i] = 0
            else:
                val = complex(output_dft[i].real, output_dft[i].imag) / complex(signal_dft[i].real, signal_dft[i].imag)
                reals[i] = val.real
                inverse[i] = val

        result = fft.ifft(inverse)
        result_magnitude = magnitude(result)

        plot_signals([signal, output], 'Original Signal', ['signal', 'output'], 'out/f/')
        plot_signals([signal_dft, output_dft], 'Frequency Domain', ['signal', 'output'], 'out/f/')
        plot_signals([reals, result_magnitude], 'Transfer Function H(u)', ['Reals', 'Magnitude'], 'out/f/')
        dual_plot(reals, result_magnitude, 'Transfer Function H(u) Dual', 'Reals', 'Magnitude', 'out/f/')

        self.log.info("")


if __name__ == '__main__':
    lab = Lab()
